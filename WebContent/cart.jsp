<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<c:if test="${empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Carrito</title>
	<style>
		body {
			margin: 30px;	
			font-family: Verdana, Geneva, sans-serif;
		}
		
		.boton {
			font-size: 1em;
			font-weight: bold;
			position: absolute;
			top: 30px; right: 40px;
			background-color: white;
			text-decoration: none;
			color: black;
			padding: 10px 18px;
			border: 2px solid lavender;
			border-radius: 4px;
			transition-duration: 0.4s;
		}
		
		.boton:hover {
			border: 2px solid black;
		  	background-color: lavender;
		}
		
		table {
			width: 50%;
			margin: 0 auto;
			margin-top: 3em;
		}
		
		th {
		  background-color: lavender;
		}
	</style>
</head>
<body>
	<%! double total=0 ;%>
	<h1>Carrito de la compra</h1>
	<hr/>
	<p><a href="catalogo.jsp" class="boton">Catálogo</a></p>
	<c:choose>
		<c:when test="${empty sessionScope.cart}">
			<p>No se han añadido productos al carrito</p>
		</c:when>
		<c:otherwise>
			<table>
				<tr><th>Producto</th><th>Unidades</th>
				<th>Precio</th><th>Importe</th></tr>
				<c:forEach var="linea" items="${sessionScope.cart}">
					<tr>
						<td>${linea.value.productName}</td>
						<td>${linea.value.amount}</td>
						<td>${linea.value.price}</td>
						<td>${linea.value.amount * linea.value.price}</td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>		
	</c:choose>
</body>
</html>
