<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<sql:query var="modelos" dataSource="jdbc/classicmodels">
	select productCode, productName, productScale, productDescription, MSRP from products; 
</sql:query>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catálogo de Productos</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
	body {
		margin: 30px;	
		font-family: Verdana, Geneva, sans-serif;
	}
	
	.boton {
		font-size: 1em;
		font-weight: bold;
		position: absolute;
		top: 30px; right: 180px;
		background-color: white;
		text-decoration: none;
		color: black;
		padding: 10px 18px;
		border: 2px solid lavender;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton2 {
		font-size: 1em;
		font-weight: bold;
		position: absolute;
		top: 30px; right: 40px;
		background-color: white;
		text-decoration: none;
		color: black;
		padding: 10px 18px;
		border: 2px solid lavender;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton2:hover, .boton:hover {
		border: 2px solid black;
	  	background-color: lavender;
	}

	h3 {
		border: 3px solid black;
		background-color: lavender;
		padding: 10px;
	}
	
	.precio {
		margin: 1em;
		text-align: right;
		font-weight: bold;
	}
</style>
</head>
<body>
	<h1>Catálogo de Modelos a Escala</h1>
	<c:choose>
		<c:when test="${not empty sessionScope.customer}">
			<p>Usuario: ${sessionScope.customer}</p>
			<p><a href="logout.jsp" class="boton">Cerrar Sesión</a></p>
			<p><a href="cart.jsp" class="boton2">Carrito</a></p>
		</c:when>
		<c:otherwise>
			<p><a href="login.jsp" class="boton">Iniciar Sesión</a></p>
			<p><a href="registro.jsp" class="boton2">Regístrate</a></p>
		</c:otherwise>
	</c:choose>
	<hr/>
	
<%  int contador = 0; %>
	
	<c:forEach var="modelo" items="${modelos.rows}">
		<% contador++; %>
	</c:forEach>
	
<%  int pags = 0;
	pags = contador / 20;
	
	int principio = 0;
	int fin = 19;
	
	if (contador % 20 != 0) {
		pags++;
		contador = pags * 20;
	}
	
	if (request.getParameter("principio") != null && request.getParameter("fin") != null) {
		
		principio = Integer.parseInt(request.getParameter("principio"));
		fin = Integer.parseInt(request.getParameter("fin"));
		boolean sirve = false;
		
		for (int i = 0; i < contador ; i += 20) {
			if (principio == i) {
				sirve = true;
			}
		}
		
		if (sirve == true) {
			sirve = false;
			
			for (int j = 19; j < contador ; j += 20) {
				if (fin == j) {
					sirve = true;
				}
			}
			
			int diferencia = fin - principio;
			
			if(fin == contador){
				sirve = true;
			}
			
			if (principio > fin || diferencia > 19) {
				sirve = false;
			}
			
			if (sirve == false) {
				principio = 0;
				fin = 19;
				response.sendRedirect("http://localhost:8083/ClassicModels/catalogo.jsp?principio=" + principio + "&fin=" + fin);
			}
			
		} else {
			principio = 0;
			fin = 19;
			response.sendRedirect("http://localhost:8083/ClassicModels/catalogo.jsp?principio=" + principio + "&fin=" + fin);
		}
	} %>

	<c:forEach var="modelo" items="${modelos.rows}" begin="<%= principio %>" end="<%= fin %>">
		<h3>
			<c:out value="${modelo.productName}" />
		</h3>
		<p>
			Escala
			<c:out value="${modelo.productScale}" />
		</p>
		<p>
			<c:out value="${modelo.productDescription}" />
		</p>
		<p class="precio">
			Precio: ${modelo.MSRP} 
			<c:if test="${not empty sessionScope.customer}">
				<a href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}">
					<img style="width: 2em; height: 2em; border: 1px solid black; margin-left: 2em;" src="img/cart-plus.svg" />
				</a>
			</c:if>
		</p>
		<hr/>
	</c:forEach>
	
	<ul class="pagination">
<% 		int primeraPag = 0;
		int ultimaPag = 19;
		
		for (int i = 1; i <= pags; i++) { %>
			<li class="page-item">
				<a class="page-link" href="catalogo.jsp?principio=<%=primeraPag%>&fin=<%=ultimaPag%>"><%=i%></a>
			</li>
<%			primeraPag += 20;
			ultimaPag += 20;
		} %>
	</ul>
	
</body>
</html>