<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>

<c:if test="${not empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inicio de Sesi�n</title>
<style>
	body {
		margin: 15px;	
		font-family: Verdana, Geneva, sans-serif;
	}

	label {
		border-bottom: 2px solid purple;
		font-weight: bold;
	}
	
	.boton {
		font-size: 1em;
		font-weight: bold;
		position: absolute;
		top: 20px; right: 40px;
		background-color: white;
		text-decoration: none;
		color: black;
		padding: 10px 18px;
		border: 2px solid lavender;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton:hover {
		border: 2px solid black;
	  	background-color: lavender;
	}
	
	.boton2  {
		background-color: white;
		color: black;
		padding: 10px 18px;
		border: 2px solid purple;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton2:hover {
	  background-color: purple;
	  color: white;
	}
	
</style>
</head>
<body>
	<h1>Inicio de Sesi�n</h1>
	<p><a href="catalogo.jsp" class="boton">Cat�logo</a></p>
	<hr />

	<form action="auth" method="post">
		
		<p>
			<label for="email">eMail</label>
		</p>
		<p>
			<input type="email" name="email" required="required" />
		</p>
		<p>
			<label for="password">Contrase�a</label>
		</p>
		<p>
			<input type="password" name="password" required="required" />
		</p>
		<p>
			<input class="boton2" type="submit" value="Login"/>
		</p>
		<c:choose>
			<c:when test="${param.status == 1}">
				<p>Las credenciales del usuario no son v�lidas</p>
			</c:when>
			<c:when test="${param.status == 2}">
				<p>Error del sistema, contacte con el administrador</p>
			</c:when>
		</c:choose>
	</form>
</body>
</html>
