<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>

<c:if test="${empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cerrar Sesi�n</title>
<style>
	body {
		margin: 15px;	
		font-family: Verdana, Geneva, sans-serif;
	}
	
	.boton {
		font-size: 1em;
		font-weight: bold;
		position: absolute;
		top: 20px; right: 40px;
		background-color: white;
		text-decoration: none;
		color: black;
		padding: 10px 18px;
		border: 2px solid lavender;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton:hover {
		border: 2px solid black;
	  	background-color: lavender;
	}
	
	.boton2  {
		background-color: white;
		color: black;
		padding: 10px 18px;
		border: 2px solid purple;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton2:hover {
	  background-color: purple;
	  color: white;
	}
	
</style>
</head>
<body>
	<h1>Cerrar Sesi�n</h1>
	<p><a href="catalogo.jsp" class="boton">Cat�logo</a></p>
	<hr />
	<p>Confirme que desea cerrar sesi�n</p>
	<form action="close" method="post">
		<p>
			<input class="boton2" type="submit" value="Logout"/>
		</p>
	</form>
</body>
</html>