<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<% if (session.getAttribute("user") != null) 
		response.sendRedirect("catalogo"); 
	else { 
		String firstName = request.getParameter("nombre"); 
		String lastName = request.getParameter("apellidos"); 
		String email = request.getParameter("email"); 
		if (email == null) 
			response.sendRedirect("catalogo.jsp"); 
		else { 
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registro</title>

<style>
	body {
		margin: 15px;	
		font-family: Verdana, Geneva, sans-serif;
	}

	label {
		border-bottom: 2px solid purple;
		font-weight: bold;
	}
	
	.boton  {
		background-color: white;
		color: black;
		padding: 10px 18px;
		border: 2px solid purple;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton:hover {
	  background-color: purple;
	  color: white;
	}
	
	.enlace {
		font-size: 1em;
		font-weight: bold;
		position: absolute;
		top: 30px; right: 230px;
		background-color: white;
		text-decoration: none;
		color: black;
		padding: 10px 18px;
		border: 2px solid lavender;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.enlace2 {
		font-size: 1em;
		font-weight: bold;
		position: absolute;
		top: 30px; right: 40px;
		background-color: white;
		text-decoration: none;
		color: black;
		padding: 10px 18px;
		border: 2px solid lavender;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.enlace2:hover, .enlace:hover {
		border: 2px solid black;
	  	background-color: lavender;
	}
	
	
</style>

</head>
	<body>
		<h1>
			Bienvenido
			<%=firstName%></h1>
		<p>
			Sólo queda un paso más para completar tu registro y tener acceso a
			todos nuestros Servicios, sigue las instrucciones que te hemos enviado
			a
			<%=email %></p>
		<p>Pulsa el botón de reenviar para que te volvamos a enviar el
			mensaje de confirmación:</p>
		<form action="reenvio" method="post">
			<input type="hidden" name="email" value="<%=email%>" />
			<p>
				<input type="submit" value="Reenviar" class="boton"/>
			</p>
		</form>
		<p>
			<a href="login.jsp" class="enlace">Iniciar sesión</a>
		</p>
		<p>
			<a href="catalogo.jsp" class="enlace2">Página de inicio</a>
		</p>
	</body>
</html>
<% 
		} 
	} 
%>