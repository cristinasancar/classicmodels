<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<c:if test="${not empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registro de Usuarios</title>

<style>
	body {
		margin: 15px;	
		font-family: Verdana, Geneva, sans-serif;
	}

	label {
		border-bottom: 2px solid purple;
		font-weight: bold;
	}
	
	.boton {
		font-size: 1em;
		font-weight: bold;
		position: absolute;
		top: 20px; right: 40px;
		background-color: white;
		text-decoration: none;
		color: black;
		padding: 10px 18px;
		border: 2px solid lavender;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton:hover {
		border: 2px solid black;
	  	background-color: lavender;
	}
	
	.boton2  {
		background-color: white;
		color: black;
		padding: 10px 18px;
		border: 2px solid purple;
		border-radius: 4px;
		transition-duration: 0.4s;
	}
	
	.boton2:hover {
	  background-color: purple;
	  color: white;
	}
	
</style>

<script>
	function check(confirm) {
		if (confirm.value != document.getElementById('password').value) {
			confirm.setCustomValidity('Las contraseñas no coinciden');
		} else
			confirm.setCustomValidity('');
	}
</script>

</head>
	<body>
		<h1>Registro de usuario</h1>
		<p><a href="catalogo.jsp" class="boton">Catálogo</a></p>
		<hr />
		<form action="signup" method="post" onsubmit="check()">
			<p>
				<label for="nombre">Nombre</label>
			</p>
			<p>
				<input type="text" name="nombre" required="required" />
			</p>
			<p>
				<label for="apellidos">Apellidos</label>
			</p>
			<p>
				<input type="text" name="apellidos" required="required" />
			</p>
			<p>
				<label for="email">eMail</label>
			</p>
			<p>
				<input type="email" name="email" required="required" />
			</p>
			<p>
				<label for="password">Contraseña</label>
			</p>
			<p>
				<input type="password" name="password" id="password"
					required="required" />
			</p>
			<p>
				<label for="password">Confirmar contraseña</label>
			</p>
			<p>
				<input type="password" id="confirm" required="required"
					oninput="check(this)" />
			</p>
			<p>
				<input type="submit" value="Login" class="boton2"/>
			</p>
		</form>
		<c:choose>
			<c:when test="${param.status == 1}">
				<p>El email ya está registro</p>
			</c:when>
			<c:when test="${param.status == 2}">
				<p>Error del sistema, contacte con el administrador</p>
			</c:when>
		</c:choose>
	</body>
</html>
