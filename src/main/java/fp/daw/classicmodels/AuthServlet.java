package fp.daw.classicmodels;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 * Servlet implementation class AuthServlet
 */
@WebServlet("/auth")
public class AuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null)
			response.sendRedirect("catalogo");
		else {
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			if (email == null || password == null)
				response.sendRedirect("login.jsp");
			else {
				int status = validateCredentials(email, password, session);
				switch (status) {
				case 0:
					response.sendRedirect("postregistro.jsp?email=" + email);
					break;		
				case 1:
					response.sendRedirect("catalogo.jsp");
					break;
				default:
					response.sendRedirect("login.jsp?status=" + status);
				}
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
	
	private int validateCredentials(String email, String password, HttpSession session) {
		Connection con = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
			con = ds.getConnection();
			stm = con.prepareStatement("select customerNumber, firstName, lastName, password, confirmationCode from signups where customerEmail = ?");
			stm.setString(1, email);
			rs = stm.executeQuery();
			if (rs.next()) {
				int customerNumber = rs.getInt(1);
				String firstName = rs.getString(2);
				String lastName = rs.getString(3);
				String hash = rs.getString(4);
				String code = rs.getString(5);
				if(code != null) {
					return 0;
				} else {
					if (HashUtils.validateBase64Hash(password, hash)) {
						session.setAttribute("customerNumber", customerNumber);
						session.setAttribute("customer", firstName + " " + lastName);
						session.setAttribute("email", email);
						return 1;
					}
					else
						return 2;
				}
		} else
			return 1;
		} catch (SQLException | NamingException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			return 2;
		} finally {
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stm != null)
				try { stm.close(); } catch (SQLException e) {}
			if (con != null)
				try { con.close(); } catch (SQLException e) {} 
		}
	}


}

