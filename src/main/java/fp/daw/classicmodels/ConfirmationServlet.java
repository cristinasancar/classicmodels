package fp.daw.classicmodels;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class ConfirmationServlet
 */
@WebServlet("/confirm")
public class ConfirmationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String email = request.getParameter("email");
		String cc = request.getParameter("cc");
			
		if ( email == null )
			response.sendRedirect("catalogo.jsp");
		else {
			
			Connection con = null;
			PreparedStatement stm = null;
			int rs = 0;
			
			try {
				Context context = new InitialContext();
				DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
				con = ds.getConnection();
				stm = con.prepareStatement("update signups set confirmationCode = ? where confirmationCode= ? and customerEmail= ?");
				stm.setNull(1, 1);
				stm.setString(2, cc);
				stm.setString(3, email);
				rs = stm.executeUpdate();
				
				if(rs > 0) {
					response.sendRedirect("catalogo.jsp");
				}
				
			} catch (SQLException e) {
				
			} catch ( NamingException e) {
	
			} finally {
				if (stm != null)
					try {
						stm.close();
					} catch (SQLException e) {
					}
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
					}
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
