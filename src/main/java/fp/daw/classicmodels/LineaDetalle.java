package fp.daw.classicmodels;

public class LineaDetalle {
	
	private String productCode;
	private String productName;
	private int amount;
	private double price;
	
	public LineaDetalle(String productCode, String productName, int amount, double price) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.amount = amount;
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getProductCode() {
		return productCode;
	}

	public String getProductName() {
		return productName;
	}

	public double getPrice() {
		return price;
	}

}
