package fp.daw.classicmodels;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailService {
	
	public static void sendMessage(String smtpServer, String port, String account, String password, String to, String subject, String body, String type) throws MessagingException {
		
		Properties properties = new Properties(); 
		// Servidor SMTP. 
		properties.setProperty("mail.smtp.host", smtpServer); 
		properties.setProperty("mail.smtp.ssl.trust", smtpServer);
		// Habilitar TLS. 
		properties.setProperty("mail.smtp.starttls.enable", "true"); 
		// Puerto para env�o de correos. 
		properties.setProperty("mail.smtp.port", port); 
		// Si requiere usuario y password para conectarse. 
		if (account != null && password != null) {
			properties.setProperty("mail.smtp.auth", "true"); 
		}
		
		// Obtener un objeto Session con las propiedades establecidas. 
		Session mailSession = Session.getInstance(properties); 
		// Para obtener un log de salida m�s extenso. 
		mailSession.setDebug(true); 
		
		Message message = new MimeMessage(mailSession); 
		message.setFrom(new InternetAddress(account)); 
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); 
		message.setSubject(subject); 
		message.setContent(body, type); 
		message.setSentDate(new Date()); 
		Transport.send(message, account, password);
	}
	
	static void sendConfirmationMessage(String email, String confirmationCode) throws MessagingException {
		StringBuilder body = new StringBuilder();
		body.append("<form action=\"http://localhost:8083/ClassicModels/confirm\"");
		body.append(" method=\"post\" style=\"text-align: center;\" >");
		body.append("<p style=\"font-weight: bold;\">Hola !!</p>");
		body.append("<p>Necesitamos confirmar tu direcci�n de correo electr�nico para completar tu registro</p>");
		body.append("<input type=\"hidden\" name=\"email\" value=\"");
		body.append(email);
		body.append("\" />");
		body.append("<input type=\"hidden\" name=\"cc\" value=\"");
		body.append(confirmationCode);
		body.append("\" />");
		body.append("<p><input type=\"submit\" value=\"Confirmar\"</p>");
		body.append("</form>");
		
		MailService.sendMessage(
			"smtp.gmail.com", // servidor SMPT
			"587", // puerto
			"classicmodelsfp@gmail.com", // cuenta para iniciar sesi�n en el servidor SMTP de Gmail
			"FP2021cifp", // contrase�a para iniciar sesi�n en el servidor SMTP de Gmail
			email, // direcci�n del destinatario
			"Confirma tu direcci�n de correo electr�nico", // asunto
			body.toString(), // cuerpo del mensaje
			"text/html" // tipo de contenido
		);
	}
}
