package fp.daw.classicmodels;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ReenvioServlet
 */
@WebServlet("/reenvio")
public class ReenvioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;     

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("usuario") != null)
			response.sendRedirect("catalogo");
		else {
			String nombre = request.getParameter("nombre");
			String apellidos = request.getParameter("apellidos");
			String email = request.getParameter("email");
			if ( email == null )
				response.sendRedirect("registro.jsp");
			else {
				switch (reenviar(email)) {
				case 0:
					response.sendRedirect("postregistro.jsp?email=" + email + "&nombre=" + nombre + "&apellidos=" + apellidos);
					break;
				case 1:
					response.sendRedirect("postregistro.jsp?status=1");
					break;
				case 2:
					response.sendRedirect("postregistro.jsp?status=2");
				}
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
	
	private int reenviar(String email) {
		int status = 0;
		try {
			String confirmationCode = HashUtils.getBase64Digest(email);
			MailService.sendConfirmationMessage(email, confirmationCode);
		} catch ( NoSuchAlgorithmException | MessagingException e) {
			status = 2;
		}
		
		return status;
	}

}
